package com.tgl.hw1.fileio;

import java.util.Comparator;

public class Employee {

	private int height;
	private int weight;
	private String ename;
	private String cname;
	private int tel;
	private String email;
	private int empid;
	private double bmi;

//	public Employee() {
//		// TODO Auto-generated constructor stub
//	}

	public Employee(int height, int weight, String ename, String cname, int tel, String email, int empid, double bmi) {
		super();
		this.height = height;
		this.weight = weight;
		this.ename = ename;
		this.cname = cname;
		this.tel = tel;
		this.email = email;
		this.bmi = bmi;
	}

	public int getHeight() {
		return this.height;
	}

	public int getWeight() {
		return this.weight;
	}

	public String getEname() {
		return this.ename;
	}

	public String getCname() {
		return this.cname;
	}

	public String getEmail() {
		return this.email;
	}

	public int getTel() {
		return this.tel;
	}

	public int getEmpid() {
		return this.empid;
	}

	public double getBmi() {
		return this.bmi;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(this.height + " ");
		result.append(this.weight + " ");
		result.append(this.ename + " ");
		result.append(this.cname + " ");
		result.append(this.tel + " ");
		result.append(this.email + " ");
		result.append(this.empid + " ");
		result.append(this.bmi + " ");
		return result.toString();
	}

	// java 1.8
	public static Comparator<Employee> heightComparator = new Comparator<Employee>() {
		@Override
		public int compare(Employee emp1, Employee emp2) {
			// return (emp2.getHeight() > emp1.getHeight() ? -1 : (emp2.getHeight() ==
			// emp1.getHeight() ? 0 : 1));
			if (emp2.getHeight() > emp1.getHeight()) {
				return -1;
			} else if (emp2.getHeight() == emp1.getHeight()) {
				return 0;
			} else {
				return 1;
			}
		}

	};

	public static Comparator<Employee> weightComparator = new Comparator<Employee>() {
		@Override
		public int compare(Employee emp1, Employee emp2) {
			// return (emp2.getWeight() > emp1.getWeight() ? -1 : (emp2.getWeight() ==
			// emp1.getWeight() ? 0 : 1));
			if (emp2.getWeight() > emp1.getWeight()) {
				return -1;
			} else if (emp2.getWeight() == emp1.getWeight()) {
				return 0;
			} else {
				return 1;
			}
		}
	};

	public static Comparator<Employee> bmiComparator = new Comparator<Employee>() {
		@Override
		public int compare(Employee emp1, Employee emp2) {
			// return (emp2.getWeight() > emp1.getWeight() ? -1 : (emp2.getWeight() ==
			// emp1.getWeight() ? 0 : 1));
			if (emp2.getBmi() > emp1.getBmi()) {
				return -1;
			} else if (emp2.getBmi() == emp1.getBmi()) {
				return 0;
			} else {
				return 1;
			}
		}

	};
}
