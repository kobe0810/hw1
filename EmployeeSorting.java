package com.tgl.hw1.fileio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeSorting {
	static List<Employee> emp = new ArrayList<>();

	public EmployeeSorting(List<Employee> emp) {
		EmployeeSorting.emp = emp;
	}

	public static List<Employee> getSortedemployeeByHeight() {
		Collections.sort(emp, Employee.heightComparator);
		return emp;
	}

	public static List<Employee> getSortedemployeeByWeight() {
		Collections.sort(emp, Employee.weightComparator);
		return emp;
	}

	public static List<Employee> getSortedemployeeByBmi() {
		Collections.sort(emp, Employee.bmiComparator);
		return emp;
	}

}
