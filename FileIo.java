package com.tgl.hw1.fileio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileIo {
	public static void main(String args[]) throws IOException {

		String str;
		int empid = 0;
		int count = 0;// 員工編號用流水序號
		double bmi = 0;

		List<Employee> list = new ArrayList<>(); // 紀錄資料用

		// 讀取檔案
		FileReader fr = new FileReader("c:\\number.txt"); // 建立FileReader物件
		BufferedReader bfr = new BufferedReader(fr); // 以fr為參數建立BufferedReader的物件bfr
		list.clear(); // list初始化

		while ((str = bfr.readLine()) != null) // readLine( )一行一行讀取資料
		{
			// 使用split處理空格分割資料
			String[] tokens = str.split(" ");
			// 每筆切割的元素 加入list
			for (String token : tokens) {
				System.out.println(token);
			}
			empid = count;
			bmi = Integer.parseInt(tokens[1])/Math.pow((Integer.parseInt(tokens[0])/100), 2);
			System.out.println(Integer.parseInt(tokens[0]));
			

			list.add(new Employee(Integer.parseInt(tokens[0]), 
					              Integer.parseInt(tokens[1]), 
					                               tokens[2], 
					                               tokens[3],
					              Integer.parseInt(tokens[4]), 
					                               tokens[5], 
					                                   empid, 
					                                     bmi));

			count++;
		}

		// 功能一 搜尋身高為171公分的同仁
		List<Employee> searchResult = new ArrayList<Employee>();// 吃結果
		EmployeeSearch search = new EmployeeSearch();
		searchResult = search.searchByHeight(list, 171);
		System.out.println("功能一結果" + searchResult.toString());

		// 功能二依體重排序
		System.out.println("Sorting By Weight: ");
		EmployeeSorting sorting = new EmployeeSorting(list);
		List<Employee> sortedEmployee = EmployeeSorting.getSortedemployeeByWeight();
		for (Employee empSort : sortedEmployee) {
			System.out.println(empSort);
		}

		// 功能三 新增一筆資料，並且顯示size
		System.out.println("--------------------------------------------------------------------------");
		fileOutputStream("c:\\number.txt", "180 80 kobe 布萊恩 2824 kobe@hotmail.com", true);
		System.out.println("list.size" + list.size());

		// 功能四 刪除身高為180的學生
		System.out.print("Remove Heigh 180");
		list.removeIf(n -> (n.getHeight() == 180));
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		// 功能五 搜尋體重最輕與最重的同仁
		Employee e = null;
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("Sorting By Max Weight: " + sortedEmployee.stream().max(e.weightComparator));
		System.out.println("Sorting By Min Weight: " + sortedEmployee.stream().min(e.weightComparator));
		System.out.println("-------------------------------------------------------------------------");
		System.out.println("Sorting By Max Bmi: " + sortedEmployee.stream().max(e.bmiComparator));
		System.out.println("Sorting By Min Bmi: " + sortedEmployee.stream().min(e.bmiComparator));

		System.out.println(count + " lines read");
		fr.close();
	}

	// 寫檔
	public static void fileOutputStream(String fileName, String data, boolean Overide) {
		try {
			BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName, Overide));
			buffer.write(data);
			buffer.newLine();
			buffer.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
