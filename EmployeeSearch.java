package com.tgl.hw1.fileio;

import java.util.ArrayList;
import java.util.List;

public class EmployeeSearch {

	public EmployeeSearch() {
		// TODO Auto-generated constructor stub
	}

	// ����
	public List<Employee> searchByHeight(List<Employee> emp, int v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {
//			System.out.println(emp.get(i));
			if (emp.get(i).getHeight() == v) {
				result.add(emp.get(i));
//				System.out.println(emp.get(i).getHeight());
			}
		}
		return result;
	}

	public List<Employee> searchByWeight(List<Employee> emp, int v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getHeight() == v) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByEname(List<Employee> emp, String v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getEname().equals(v)) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByCname(List<Employee> emp, String v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getCname().equals(v)) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByTel(List<Employee> emp, int v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getTel() == v) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByEmail(List<Employee> emp, String v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getEmail().equals(v)) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByEmpid(List<Employee> emp, int v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getEmpid() == v) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

	public List<Employee> searchByBmi(List<Employee> emp, int v) {
		List<Employee> result = new ArrayList<Employee>();
		for (int i = 0; i < emp.size(); i++) {

			if (emp.get(i).getBmi() == v) {
				result.add(emp.get(i));

			}
		}
		return result;
	}

}
